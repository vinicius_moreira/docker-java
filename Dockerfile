FROM tomcat:8-jre8
MAINTAINER "Vinícius Moreira <vinicius_fmoreira@hotmail.com>"
ADD tomcat-docker/tomcat-users.xml /usr/local/tomcat/conf/
ADD target/docker-java.war /usr/local/tomcat/webapps/
