HOW TO EXECUTE IT:
------------------
You must have Docker, Docker-Compose and Maven installed in your machine. (when I made this up, I used Docker 1.9.1, Docker-Compose 1.5.2, Maven 3.3.1 and JDK 1.8.0_60)

Open a terminal application and go to the project's root folder:

1) You must compile the project using Maven (lots of ways to do that... a simple one is 'mvn clean install')
2) Build the project's docker image. You can accomplish that by executing 'docker build -t "docker-java" .' (UNIX based system might need 'sudo' in the beginning)
3) Ask docker-compose to run the containers (postgres and the app). Simple way: 'docker-compose up' (might need 'sudo'). When the logs stop running, go to the next step.

Open a browser:

1) Type: 'http://localhost:8080/docker-java/generate'. Every time that you call this URL, the application will generate a random number from 0 to 50.
2) Type: 'http://localhost:8080/docker-java/list'. This call will list every generated random number (They are stored in Postgres)
