package moreira.vinicius.docker.controller;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import moreira.vinicius.docker.domain.GeneratedRandom;
import moreira.vinicius.docker.domain.GeneratedRandomRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RandomController {

	@Autowired
	private GeneratedRandomRepository repository;
	
	@RequestMapping(value="/generate")
	@Transactional
	public String generate(HttpServletRequest request){
		int random = new Random().nextInt(50);
		String ip = request.getRemoteAddr();
		this.repository.save(new GeneratedRandom(random, ip));
		return String.valueOf(random);
	}
	
	@RequestMapping(value="/list")
	public Iterable<GeneratedRandom> list(){
		return this.repository.findAll(new Sort(Direction.DESC,"date"));
	}
}