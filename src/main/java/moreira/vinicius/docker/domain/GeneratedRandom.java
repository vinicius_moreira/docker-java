package moreira.vinicius.docker.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@EqualsAndHashCode(of="id")
@Getter
@NoArgsConstructor(access=AccessLevel.PRIVATE)
public class GeneratedRandom implements Serializable {

	/**	 */
	private static final long serialVersionUID = -1654871946362661710L;

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private Integer random;
	
	@Column(nullable = false)
	@JsonSerialize(using=ToStringSerializer.class)
	private Date date;
	
	@Column(nullable = false)
	private String ip;
	
	public GeneratedRandom(Integer random, String ip) {
		this.date = new Date();
		this.random = random;
		this.ip = ip;
	}
}