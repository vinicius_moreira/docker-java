package moreira.vinicius.docker.domain;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface GeneratedRandomRepository extends PagingAndSortingRepository<GeneratedRandom, Long> {

}
